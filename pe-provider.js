import { ApolloLink } from 'apollo-link'
// import {RestLink} from "apollo-link-rest";
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { persistCache } from 'apollo-cache-persist'
import { Notify } from 'quasar'
import i18next from 'i18next'
import ru from '../i18n/ru'

const i18n = i18next.init({
  lng: 'ru',
  debug: true,
  keySeparator: '????????????',
  nsSeparator: '????????????',
  resources: {
    ru: {
      translation: ru
    }
  }
})

function error (message) {
  i18n.then(t => Notify.create({ type: 'negative', message: t(message) }))
}

function internalError (message, applicationType) {
  i18n.then(t => {
    Notify.create({
      type: 'negative',
      message: t(applicationType) + ': ' +
        t(message) + '. ' +
        t('Contact to support service')
    })
  })
}

// import { createUploadLink } from 'apollo-upload-client'

import config from '../config/config'

const myLocalStorage = {
  getItem: key => window.localStorage.getItem(key),
  setItem: (key, value) => {
    if (key === 'apollo-cache-persist') {
      const data = JSON.parse(value)
      for (const k in data) {
        if (k.startsWith('ROOT_MUTATION') || k.startsWith('$ROOT_MUTATION')) {
          delete data[k]
        }
      }
      value = JSON.stringify(data)
    }
    window.localStorage.setItem(key, value)
  }
}

function peApolloClientConfig (pwa) {
  const queryLink = new HttpLink({ uri: config.server_url })
  const cache = new InMemoryCache()
  if (pwa) {
    persistCache({
      cache,
      storage: myLocalStorage
    })
  }
  const authLink = setContext((_, prevContext) => {
    console.log(prevContext)
    const headers = prevContext.headers
    let uri = config.server_url
    if (prevContext.uri) {
      uri = prevContext.uri
    }
    let newHeaders
    if (localStorage.getItem('user_token')) {
      const token = localStorage.getItem('user_token') ? localStorage.getItem('user_token') : ''
      newHeaders = {
        ...headers,
        Authorization: 'Bearer ' + token
      }
    } else if (localStorage.getItem('client_token')) {
      const token = localStorage.getItem('client_token') ? localStorage.getItem('client_token') : ''
      newHeaders = {
        ...headers,
        Authorization: 'Bearer ' + token
      }
    } else {
      newHeaders = {
        ...headers
      }
    }

    // "authorization": 'Bearer ' + token,

    // return the headers to the context so httpLink can read them
    // const xxx = base64_encode( login +':'+ password );
    // console.log(new_headers);
    return {
      headers: newHeaders,
      uri: uri,
      application_type: prevContext.application_type ? prevContext.application_type : 'auth_server'
    }
  })

  //                    AppToaster.show({
  //                         intent: get_msg_types()[msg_type].intent,
  //                         icon: get_msg_types()[msg_type].icon,
  //                         timeout:10000,
  //                         className: "px-2 py-2",
  //                         message: __.t(data.msg),
  //                     });

  const errorlink = onError(({ graphQLErrors, networkError, operation, forward }) => {
    if (networkError && networkError.statusCode !== 400) {
      internalError('Network error. Can\'t connect to server', operation.getContext().application_type)
    } else if (graphQLErrors) {
      for (const err of graphQLErrors) {
        if (err.extensions) {
          switch (err.extensions.code) {
            case 'UNAUTHENTICATED':
            case 'INTERNAL_SERVER_ERROR':
              internalError(err.message, operation.getContext().application_type)
              break
            // case 'FORBIDDEN':
            //   console.log(err)
            //   break
            default:
              // AppToaster.show({
              //     intent: Intent.DANGER,
              //     icon: "error",
              //     message: __( err.message )
              // });
              error(err.message)
              break
          }
        } else {
          // AppToaster.show({
          //     intent: Intent.WARNING,
          //     icon: "warning",
          //     message: __( err.message )
          // });
          error(err.message)
        }
      }
    }
  })

  const policy = pwa ? {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'cache-and-network'
  } : {}

  const defaultOptions = {
    watchQuery: policy,
    query: {}
  }

  return {
    link: ApolloLink.from([
      errorlink,
      authLink,
      queryLink

    ]),
    // httpLinkConfig: { uri: config.server_url },
    // authLinkConfig: authLink,
    cache: cache,
    defaultHttpLink: false,
    defaultOptions: defaultOptions,
    fetchOptions: {
      credentials: 'include',
      mode: 'no-cors'
    }

  }
}

export default peApolloClientConfig
